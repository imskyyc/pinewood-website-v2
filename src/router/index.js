import Vue from "vue";
import VueRouter from "vue-router";

const HomeView = () =>
  import(/* webpackChunkName: "home" */ "../views/HomeView.vue");

const Error404 = () =>
  import(/* webpackChunkName: "error-404" */ "../views/404Error.vue");

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: HomeView,
  },

  {
    path: "*",
    name: "404",
    component: Error404,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = `Pinewood Builders - ${to.name}`;

  next();
});

export default router;
